package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead2 extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead-NegativeCases";
		testDescription = "CreateLead";
		authors = "Poornima Devi";
		category = "smoke";
		dataSheetName = "TC002_CreateLeadNegative";
		testNodes = "Leads";
	}

	@Test(dataProvider = "fetchData")
	public void createLead(String userName, String password, String companyName, String firstName, String lastName) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfaLink()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickCreateLeadFailure()
		.verifyParamterMissing();

		/*
		 * LoginPage lp = new LoginPage(); lp.enterUserName(); lp.enterPassword();
		 * lp.clickLogin();
		 */
	}

}

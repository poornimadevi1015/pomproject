package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.FirstProduct;
import pages.FlipKartHomePage;
import pages.LoginPage;
import wdMethods.ProjectMethods;
import wdMethods.ProjectMethodsFlipKart;

public class TC_FlipKart extends ProjectMethodsFlipKart {
	@BeforeTest
	public void setData() {
		testCaseName = "TC_FlipKart";
		testDescription = "To select the first product name in the MI brand";
		authors = "Poornima Devi";
		category = "Functionality";
		dataSheetName = "FlipKartData";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void selectFirstProduct(String currentTitle) throws InterruptedException {
		
		String text= new FlipKartHomePage()
		.closePopUP()
		.mouseHoverElectronics()
		.clickMi()
		.clickNewestFirst()
		.getAllProductName()
		.getAllProductPrice()
		.clickFirstProduct()
		.verifyCurrentTitle(currentTitle);
		new FirstProduct()
		.countRate()
		.countReview();
		
		
	}

	/*@Test(dataProvider = "fetchData")
	public void createLead(String userName, String password, String companyName, String firstName, String lastName) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfaLink()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickSubmitCreateLead()
		.verifyFN(firstName);

		
		 * LoginPage lp = new LoginPage(); lp.enterUserName(); lp.enterPassword();
		 * lp.clickLogin();
		 
	}*/

}

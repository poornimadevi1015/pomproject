package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_MergeLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC004_MergeLead";
		testDescription = "mergeLead";
		authors = "Poornima Devi";
		category = "smoke";
		dataSheetName = "TC004_MergeLead";
		testNodes = "Leads";
	}

	@Test(dataProvider = "fetchData")
	public void createLead(String userName, String password, String mergeFromFirstName, String mergeToFirstName) throws InterruptedException {

		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfaLink()
		.clickLeads()
		.clickMergeLead()
		.clickFromLeadIcon()
		.mergeFromFirstName(mergeFromFirstName)
		.clickFindLeadButton()
		.clickFirstLeadList()
		.clickToLeadIcon()
		.mergeToFirstName(mergeToFirstName)
		.clickFindLeadButton()
		.clickFirstLeadList()
		.clickMergeLeadButton();
		
		/*
		 * LoginPage lp = new LoginPage(); lp.enterUserName(); lp.enterPassword();
		 * lp.clickLogin();
		 */
	}

}

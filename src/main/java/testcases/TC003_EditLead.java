package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_EditLead";
		testDescription = "EditLead";
		authors = "Poornima Devi";
		category = "smoke";
		dataSheetName = "TC003_EditLead";
		testNodes = "Leads";
	}

	@Test(dataProvider = "fetchData")
	public void createLead(String userName, String password, String companyName, String firstName, String lastName,
			String modifyFirstName, String modifyLastName, String modifyCompanyName) throws InterruptedException {

		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfaLink()
		.clickLeads()
		.clickEditLead()
		.enterFirstName(firstName)
		.clickFindLeadButton()
		.clickFirstResult()
		.clickEdit()
		.modifyFirstName(modifyFirstName)
		.modifyLastName(modifyLastName)
		.modifyCompanyName(modifyCompanyName)
		.clickUpdate();

		/*
		 * LoginPage lp = new LoginPage(); lp.enterUserName(); lp.enterPassword();
		 * lp.clickLogin();
		 */
	}

}

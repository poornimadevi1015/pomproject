package pages;

import java.lang.annotation.Repeatable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}

	// To verify the first name

	@FindBy(how = How.ID_OR_NAME, using = "viewLead_firstName_sp")
	WebElement verifyValueFN;

	@Then("Verify whether the created lead is created successfully or not")
	public ViewLeadPage verifyFN(String data) {

		verifyExactText(verifyValueFN, data);

		return this;

	}

	// To click on the Edit button
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Edit')]")
	WebElement eleEditButton;
 @When("Click on the edit button")
	public EditLeadPage clickEdit() {

		click(eleEditButton);

		return new EditLeadPage();

	}
	
	@Then("Verify whether it is navigated to the view lead page")
	
	public ViewLeadPage verifyViewLeadPage() {
		
		System.out.println("View Lead Page");
		
		return this;
	}

}

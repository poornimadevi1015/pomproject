package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods {

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}

	// To modify the company name

	@FindBy(how = How.ID_OR_NAME, using = "updateLeadForm_companyName")
	WebElement enterCN;

	public EditLeadPage modifyCompanyName(String data) {

		type(enterCN, data);

		return this;

	}

	// To modify the first name

	@FindBy(how = How.ID_OR_NAME, using = "updateLeadForm_firstName")
	WebElement enterFN;

	@And("Modify the first name as (.*)")
	public EditLeadPage modifyFirstName(String data) {

		type(enterFN, data);

		return this;

	}

	// To modify the last name

	@FindBy(how = How.ID_OR_NAME, using = "updateLeadForm_lastName")
	WebElement enterLN;

	public EditLeadPage modifyLastName(String data) {

		type(enterLN, data);

		return this;

	}

	// To click on the update button

	@FindBy(how = How.XPATH, using = "//input[@class='smallSubmit']")
	WebElement eleUpdateButton;

	@When("Click on the update button")
	public ViewLeadPage clickUpdate() {

		click(eleUpdateButton);

		return new ViewLeadPage();

	}

}

package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MyHomePage extends ProjectMethods {

	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}

	// To click on the Lead button

	@FindBy(how = How.LINK_TEXT, using = "Leads")
	WebElement eleLeads;

	@When("Click on the Leads")
	public MyLeadPage clickLeads() {

		click(eleLeads);
		
		return new MyLeadPage();

	}
	
	@Then("Verify whether it is navigated to the my home page")
	
	public MyHomePage verifyMyHomePage() {
		
		System.out.println("MyHomePage");
		
		return this;
		
	}

}

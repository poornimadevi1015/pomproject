package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;
import wdMethods.ProjectMethodsFlipKart;

public class FlipKartHomePage extends ProjectMethodsFlipKart {

	public FlipKartHomePage() {

		PageFactory.initElements(driver, this);

	}

	public FlipKartHomePage closePopUP() {

		closePopUp();

		return this;

	}

	@FindBy(how = How.XPATH, using = "//li[@class='Wbt_B2 _1YVU3_']")
	WebElement eleElectronics;

	public FlipKartHomePage mouseHoverElectronics() {

		mouseHover(eleElectronics);

		return this;

	}

	@FindBy(how = How.XPATH, using = "//a[text()='Mi']")
	WebElement eleMi;

	public MiPage clickMi() throws InterruptedException {

		clickWithNoSnap(eleMi);

		Thread.sleep(2000);
	
		System.out.println(getTitle());

		return new MiPage();
		
	}
	

	
	

}

package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MiPage extends ProjectMethods {

	public MiPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//div[text()='Newest First']")
	WebElement eleNewestFirst;

	public MiPage clickNewestFirst() throws InterruptedException {

		click(eleNewestFirst);

		Thread.sleep(2000);

		return this;

	}

	@FindBy(how = How.XPATH, using = "//div[@class='_3wU53n']")
	List<WebElement> eleProductName;

	public MiPage getAllProductName() {

		for (WebElement listProductName : eleProductName) {

			System.out.println(listProductName.getText());

		}

		return this;

	}

	@FindBy(how = How.XPATH, using = "//div[@class='_1vC4OE _2rQ-NK']")
	List<WebElement> eleProductPrice;

	public MiPage getAllProductPrice() {

		for (WebElement listProductPrice : eleProductPrice) {

			System.out.println(listProductPrice.getText());

		}

		return this;

	}
	
	
@FindBy(how=How.XPATH,using="(//div[@class='_3wU53n'])[1]") WebElement eleFirstProduct;
	
	public FirstProduct clickFirstProduct() throws InterruptedException {
		
		click(eleFirstProduct);
		
		windowHandles();
		
		Thread.sleep(2000);
		
		
		return new FirstProduct();


		
	}
	


}

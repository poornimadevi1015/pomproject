package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit")
	WebElement eleLogout;

	public LoginPage clickLogout() {
		click(eleLogout);
		return new LoginPage();
	}

	// To click on the crmsfa link

	@FindBy(how = How.LINK_TEXT, using = "CRM/SFA")
	WebElement eleCrmClick;
 
	@When("Click on the crmsfa link")
	
	public MyHomePage clickCrmSfaLink() {

		click(eleCrmClick);
		
		return new MyHomePage();

	}
	
	@Then("Verify whether it is navigated to the home page")
	public HomePage verifyHomePage() {
		
		System.out.println("Home Page");
		
		return this;
		
	}

}

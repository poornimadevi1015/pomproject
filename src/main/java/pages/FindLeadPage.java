package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods {

	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	}

	// To enter the first name

	@FindBy(how = How.XPATH, using = "(//input[@name='firstName'])[3]")
	WebElement eleFirstName;

	@And("Provide the first name as (.*) in the find lead page")
	public FindLeadPage enterFirstName(String data) {

		type(eleFirstName, data);

		return this;

	}

	// To enter the last name

	@FindBy(how = How.XPATH, using = "(//input[@name='lastName'])[3]")
	WebElement eleLastName;

	public FindLeadPage enterLastName(String data) {

		type(eleLastName, data);

		return this;

	}

	// To enter the company name

	@FindBy(how = How.XPATH, using = "(//input[@name='companyName'])[2]")
	WebElement eleCompanyName;

	public FindLeadPage enterCompanyName(String data) {

		type(eleCompanyName, data);

		return this;

	}

	// To enter the value in the first name - from lead

	@FindBy(how = How.XPATH, using = "//input[@name = 'firstName']")
	WebElement mergeFromName;
	@And("Provide the first name as (.*) in the from box")
	public FindLeadPage mergeFromFirstName(String data) {

		type(mergeFromName, data);

		return this;

	}

	// To enter the value in the first name -To lead

	@FindBy(how = How.XPATH, using = "//input[@name = 'firstName']")
	WebElement mergeToName;
	@And("Provide the first name as (.*) in the to icon box")
	public FindLeadPage mergeToFirstName(String data) {

		type(mergeToName, data);

		return this;

	}

	// To click on the first lead list

	@FindBy(how = How.CLASS_NAME, using = "linktext")
	WebElement eleFirstLeadList;
	@When("Click on the first resulting lead in the merge lead")
	public MergeLeadPage clickFirstLeadList() {

		clickWithNoSnap(eleFirstLeadList);

		switchToWindow(0);

		return new MergeLeadPage();

	}

	// To click on the Find Lead button

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Find Leads')]")
	WebElement eleFindLeadButton;
	
	@When("Click on the find lead button")

	public FindLeadPage clickFindLeadButton() throws InterruptedException {

		click(eleFindLeadButton);

		Thread.sleep(2000);

		return this;

	}

	// To click on the First Resulting lead

	@FindBy(how = How.XPATH, using = "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	WebElement eleFirstResult;
	
	@And("Click on the first resulting lead")

	public ViewLeadPage clickFirstResult() {

		click(eleFirstResult);

		return new ViewLeadPage();

	}

	@Then("Verify whether it is navigated to the find lead page")
	
	public FindLeadPage verifyFindLeadPage() {

		System.out.println("Find Lead Page");

		return this;

	}

}

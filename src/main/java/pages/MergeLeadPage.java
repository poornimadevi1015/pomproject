package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods {

	public MergeLeadPage() {
		PageFactory.initElements(driver, this);
	}

	// To click on the from lead icon

	@FindBy(how = How.XPATH, using = "(//img[@alt = 'Lookup'])[1]")
	WebElement eleClickFromLead;

	@When("Click on the from icon")
	public FindLeadPage clickFromLeadIcon() {

		clickWithNoSnap(eleClickFromLead);

		switchToWindow(1);

		return new FindLeadPage();

	}

	// To click on the To lead icon

	@FindBy(how = How.XPATH, using = "(//img[@alt = 'Lookup'])[2]")
	WebElement eleClickToLead;

	@When("Click on the to icon")
	public FindLeadPage clickToLeadIcon() {

		clickWithNoSnap(eleClickToLead);

		switchToWindow(1);

		return new FindLeadPage();

	}

	// To enter the first name

	@FindBy(how = How.XPATH, using = "(//input[@name='firstName'])[3]")
	WebElement eleFirstName;

	

	public MergeLeadPage enterFirstName(String data) {

		type(eleFirstName, data);

		return this;

	}

	// To enter the last name

	@FindBy(how = How.XPATH, using = "(//input[@name='lastName'])[3]")
	WebElement eleLastName;

	public MergeLeadPage enterLastName(String data) {

		type(eleLastName, data);

		return this;

	}

	// To enter the company name

	@FindBy(how = How.XPATH, using = "(//input[@name='companyName'])[2]")
	WebElement eleCompanyName;

	public MergeLeadPage enterCompanyName(String data) {

		type(eleCompanyName, data);

		return this;

	}

	// To click on the Find Lead button

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Find Leads')]")
	WebElement eleFindLeadButton;

	@When("Click on the find lead button in the merge lead")
	public MergeLeadPage clickFindLeadButton() throws InterruptedException {

		click(eleFindLeadButton);

		Thread.sleep(2000);

		return this;

	}

	// To click on the First Resulting lead

	@FindBy(how = How.XPATH, using = "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	WebElement eleFirstResult;

	
	public ViewLeadPage clickFirstResult() {

		click(eleFirstResult);

		return new ViewLeadPage();

	}

	// To click on the merge lead icon

	@FindBy(how = How.CLASS_NAME, using = "buttonDangerous")
	WebElement eleClickMergeLead;
@When("Click on the merge button")
	public ViewLeadPage clickMergeLeadButton() {

		clickWithNoSnap(eleClickMergeLead);

		acceptAlert();

		return new ViewLeadPage();

	}

	@Then("Verify whether it is navigated to the merge lead page")
	public MergeLeadPage verifyMergeLeadPage() {

		System.out.println("Merge Lead Page");

		return this;
	}

}

package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {  
		PageFactory.initElements(driver, this);
	}

	// To enter the company name

	@FindBy(how = How.ID_OR_NAME, using = "createLeadForm_companyName")
	WebElement enterCN;
   
	@And("Enter the company name as (.*)")
	public CreateLeadPage enterCompanyName(String data) {

		type(enterCN, data);

		return this;

	}

	// To enter the first name

	@FindBy(how = How.ID_OR_NAME, using = "createLeadForm_firstName")
	WebElement enterFN;
   
	@And("Enter the first name as (.*)")
	public CreateLeadPage enterFirstName(String data) {

		type(enterFN, data);

		return this;

	}

	// To enter the last name

	@FindBy(how = How.ID_OR_NAME, using = "createLeadForm_lastName")
	WebElement enterLN;

	@And("Enter the last name as (.*)")
	public CreateLeadPage enterLastName(String data) {

		type(enterLN, data);

		return this;

	}

	// To click on the create Lead Form

	@FindBy(how = How.NAME, using = "submitButton")
	WebElement eleCreateLead;

	@When("Click on the create lead button")
	public ViewLeadPage clickSubmitCreateLead() {

		click(eleCreateLead);

		return new ViewLeadPage();

	}

	@FindBy(how = How.NAME, using = "submitButton")
	WebElement eleCreateLeadFailure;

	public CreateLeadPage clickCreateLeadFailure() {

		click(eleCreateLeadFailure);

		return this;

	}

	// To verify the error message

	@FindBy(how = How.CLASS_NAME, using = "errorMessage")
	WebElement eleMissing;

	public void verifyParamterMissing() {

		verifyPartialText(eleMissing, " parameter is missing");
	}

	@Then("Verify whether it is navigated to the Create lead page")
	public CreateLeadPage verifyCreateLeadPage() {
		
		System.out.println("CreateLeadage");
		
		return this;
	}
}

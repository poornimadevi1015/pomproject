package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MyLeadPage extends ProjectMethods {

	public MyLeadPage() {
		PageFactory.initElements(driver, this);
	}

	// To click on the Create Lead button

	@FindBy(how = How.LINK_TEXT, using = "Create Lead")
	WebElement eleCreateLead;

	@When("Click on the create lead link")
	public CreateLeadPage clickCreateLead() {

		click(eleCreateLead);

		return new CreateLeadPage();

	}

	// To click on the find lead button

	@FindBy(how = How.LINK_TEXT, using = "Find Leads")
	WebElement eleClickFindLead;

	@When("Click on the find lead link")
	public FindLeadPage clickEditLead() {

		click(eleClickFindLead);

		return new FindLeadPage();

	}

	// To click on the merge lead button

	@FindBy(how = How.LINK_TEXT, using = "Merge Leads")
	WebElement eleClickMergeLead;
@When("Click on the merge lead link")
	public MergeLeadPage clickMergeLead() {

		click(eleClickMergeLead);

		return new MergeLeadPage();

	}
	
	@Then("Verify whether it is navigated to the lead page")
	
	public MyLeadPage verifyLeadPage() {
		
		System.out.println("Lead Page");
		
		return this;
	}

}

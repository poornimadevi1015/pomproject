package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class FirstProduct extends ProjectMethods {

	public FirstProduct() {
		PageFactory.initElements(driver, this);
	}

	public String verifyCurrentTitle(String data) {

		verifyTitle(data);
		
		return data;

	}

	@FindBy(how = How.XPATH, using = "//div[text()='Ratings & Reviews']/following::span[2]")
	WebElement countRate;

	public FirstProduct countRate() {

		System.out.println(countRate.getText());

		return this;

	}

	@FindBy(how = How.XPATH, using = "//div[text()='Ratings & Reviews']/following::span[2]")
	WebElement countReviews;

	public FirstProduct countReview() {

		System.out.println(countReviews.getText());

		return this;

	}
	
	
	
}

package run;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;

import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(features = "src\\test\\java\\features\\EditLead.Feature", glue = {"steps","pages"}, monochrome = true)

public class EditLeadRun {

}

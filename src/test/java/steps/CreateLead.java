/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {

	public ChromeDriver driver;

	String firstName1;

	@Given("Open the browser")
	public void openTheBrowser() {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		driver = new ChromeDriver();

	}

	@Given("Max the browser")
	public void maxTheBrowser() {

		driver.manage().window().maximize();

	}

	@Given("Set the timeout")
	public void setTheTimeout() {

		driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);

	}

	@Given("Launch the url as (.*)")
	public void launchTheUrl(String url) {

		driver.get(url);

	}

	@Given("Enter the user name as (.*)")
	public void enterTheUserName(String userName) {

		driver.findElementById("username").sendKeys(userName);

	}

	@Given("Enter the password as (.*)")
	public void enterThePassword(String passWord) {

		driver.findElementById("password").sendKeys(passWord);

	}

	@When("Click on the login button")
	public void clickOnTheLoginButton() {

		driver.findElementByClassName("decorativeSubmit").click();

	}

	@Then("Verify whether it is navigated to the home page")
	public void verifyWhetherItIsNavigatedToTheHomePage() {

		System.out.println("Navigated to the home page");

	}

	@When("Click on the crmsfa link")
	public void clickOnTheCrmsfaLink() {

		driver.findElementByLinkText("CRM/SFA").click();

	}

	@Then("Verify whether it is navigated to the my home page")
	public void verifyWhetherItIsNavigatedToTheMyHomePage() {

		System.out.println("Navigated to the my home page");

	}

	@When("Click on the Leads")
	public void clickOnTheLeads() {

		driver.findElementByLinkText("Leads").click();

	}

	@Then("Verify whether it is navigated to the lead page")
	public void verifyWhetherItIsNavigatedToTheLeadPage() {

		System.out.println("Navigated to the My Lead Page");

	}

	@When("Click on the create lead link")
	public void clickOnTheCreateLeadLink() {

		driver.findElementByLinkText("Create Lead").click();

	}

	@Then("Enter the company name as (.*)")
	public void enterTheCompanyName(String companyName) {

		driver.findElementById("createLeadForm_companyName").sendKeys(companyName);

	}

	@Then("Enter the first name as (.*)")
	public void enterTheFirstName(String firstName) {

		firstName1 = firstName;
		driver.findElementById("createLeadForm_firstName").sendKeys(firstName);

	}

	@Then("Enter the last name as (.*)")
	public void enterTheLastName(String lastName) {

		driver.findElementById("createLeadForm_lastName").sendKeys(lastName);

	}

	@When("Click on the create lead button")
	public void clickOnTheCreateLeadButton() {

		driver.findElementByName("submitButton").click();

	}

	@Then("Verify whether it is navigated to the view lead page")
	public void verifyWhetherItIsNavigatedToTheViewLeadPage() {

		System.out.println("Navigated to the View Lead Page");

	}

	@Then("Verify whether the created lead is created successfully or not")
	public void verifyWhetherTheCreatedLeadIsCreatedSuccessfullyOrNot() {

		if (firstName1.equalsIgnoreCase(driver.findElementById("viewLead_firstName_sp").getText())) {
			System.out.println("Successfully created");
		}

	}

	@Then("Close the browser")
	public void closeThrBrowser() {

		driver.close();

	}
}
*/
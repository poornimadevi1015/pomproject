package steps;

import cucumber.api.Result.Type;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class HooksClass extends SeMethods{

	@Before

	public void beforeScenario(Scenario sc) {

		/*String testCastName = sc.getName();

		System.out.println(testCastName);

		String iterator = sc.getId();

		System.out.println(iterator);
		
		String uri = sc.getUri();
		
		System.out.println(uri);*/
		
		startResult();
		startTestModule(sc.getName(), sc.getId());	
		test = startTestCase("Nodes");
		test.assignCategory("Functional");
		test.assignAuthor("Poornima Devi");
		startApp("chrome", "http://leaftaps.com/opentaps");		

	}

	@After
	
	public void afterScenario(Scenario sc) {

		/*Type status = sc.getStatus();

		System.out.println(status);

		boolean failed = sc.isFailed();

		System.out.println(failed);*/
		
		closeAllBrowsers();
		
		endResult();

	}

}
